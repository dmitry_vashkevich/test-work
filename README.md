# README #

#### Запуск:
    git clone git@bitbucket.org:dmitry_vashkevich/test-work.git
    cd test-work
    sudo docker-compose up
    
##### Дополнительно:
- Добавить в /etc/hosts (хост можно поменять в docker/nginx/nginx.conf):

        127.0.0.1 maxline

- Запустить установку composer в контейнере php:

        sudo docker exec -it <container_id> sh
        composer update
        
- Получить id контейнера можно командой:

        sudo docker container ps

- После этого сервис будет доступен по адресу http://maxline:8080/

#### Доступные ресурсы:

* Получение погоды по параметрам:
    * GET /api/weather/?city=London
    * GET /api/weather/?coords=53.333,27.01
    * GET /api/weather/?url=https://www.google.com/maps/@53.4318554,27.0034327,8z?hl=en
    
* Получение списка городов, введенных ранее:
    * GET /api/weather/cityList