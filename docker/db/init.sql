CREATE DATABASE IF NOT EXISTS `weather`;
USE `weather`;

CREATE TABLE `city` (
  `name` VARCHAR(250) NOT NULL,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC)
);
