<?php

class CityStorage
{
    public function add($city)
    {
        $cityModel = new City;
        $cityModel->name = $city;
        $cityModel->save();
    }

    public function getAll() {
        $data = [];
        foreach (City::find() as $city) {
            $data[] = $city->name;
        }
        return $data;
    }
}
