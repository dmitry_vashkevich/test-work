<?php

class WeatherService
{
    private $apiUrl = 'http://api.openweathermap.org/data/2.5/weather';
    private $apiAppId;
    private $client;

    public function __construct($config)
    {
        $this->apiAppId = $config->openweather_api['app_id'];
        $this->client = new GuzzleHttp\Client();
    }

    /**
     * @param string $city
     * @return string
     */
    public function getWeatherByCity($city)
    {
        $res = $this->client->request('GET', $this->apiUrl, [
            'query' => [
                'q' => $city,
                'APPID' => $this->apiAppId
            ]
        ]);
        return $res->getBody()->getContents();
    }

    /**
     * @param string $coords
     * @return string
     */
    public function getWeatherByCoords($coords)
    {
        list($lat, $lon) = explode(',', $coords);
        $res = $this->client->request('GET', $this->apiUrl, [
            'query' => [
                'lat' => $lat,
                'lon' => $lon,
                'APPID' => $this->apiAppId
            ]
        ]);
        return $res->getBody()->getContents();
    }

    /**
     * Support only urls like google maps
     *
     * @param string $url
     * @return string
     * @throws Exception
     */
    public function getWeatherByUrl($url)
    {
        if (preg_match('/@[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)/', $url, $out)) {
            $coords = substr($out[0], 1);
            return $this->getWeatherByCoords($coords);
        }
        throw new Exception('Invalid url');
    }

    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public function getWeather($params)
    {
        try {
            if ($params['city'] !== null) {
                return $this->getWeatherByCity($params['city']);
            }
            if ($params['coords'] !== null) {
                return $this->getWeatherByCoords($params['coords']);
            }
            if ($params['url'] !== null) {
                return $this->getWeatherByUrl($params['url']);
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $message = str_replace($this->apiAppId, '__PROTECTED__', $message);
            throw new Exception($message);
        }
        throw new Exception('Params Incorrect');
    }
}
