<?php

class CityCache
{
    private $redis;
    private $prefix = 'cityCache:';

    public function __construct($config)
    {
        $redis = new \Redis;
        $redis->connect($config->redis->host, $config->redis->port);
        $redis->setOption(Redis::OPT_PREFIX, $this->prefix);
        if (isset($config->redis->auth)) {
            $redis->auth($config->redis->auth);
        }
        $this->redis = $redis;
    }

    public function add($city)
    {
        $this->redis->sAdd('city', $city);
    }

    public function getAll()
    {
        return $this->redis->sMembers('city');
    }
}
