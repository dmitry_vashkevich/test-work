<?php

use Phalcon\Mvc\Controller;

class ApiController extends Controller
{
    /**
     * Return weather by params
     *
     * GET /api/weather/?city=London
     * GET /api/weather/?coords=53.333,27.01
     * GET /api/weather/?url=https://www.google.com/maps/@53.4318554,27.0034327,8z?hl=en
     */
    public function weatherAction()
    {
        /**
         * @var $cityCache CityCache
         * @var $cityStorage CityStorage
         * @var $weatherService WeatherService
         */
        $cityCache = $this->di->get('cityCache');
        $cityStorage = $this->di->get('cityStorage');
        $weatherService = $this->di->get('weatherService');

        //save city in cache (redis) and storage
        if (null !== ($city = $this->request->get('city'))) {
            $cityStorage->add($city);
            $cityCache->add($city);
        }

        try {
            //get weather by all possible params
            $weather = $weatherService->getWeather([
                'city' => $this->request->get('city'),
                'coords' => $this->request->get('coords'),
                'url' => $this->request->get('url')
            ]);
            $this->response->setJsonContent(json_decode($weather));
        } catch (Exception $e) {
            $this->response->setJsonContent(['error' => $e->getMessage()]);
        }
        return $this->response;
    }

    /**
     * Return array of city from database
     *
     * GET /api/cityList
     */
    public function cityListAction()
    {
        $cityStorage = $this->di->get('cityStorage');

        $cityList = $cityStorage->getAll();

        $this->response->setJsonContent($cityList);
        return $this->response;
    }
}
